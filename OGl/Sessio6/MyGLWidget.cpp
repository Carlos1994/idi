#include "MyGLWidget.h"

#include <iostream>

MyGLWidget::MyGLWidget (QWidget* parent) : QOpenGLWidget(parent)
{
  setFocusPolicy(Qt::ClickFocus);  // per rebre events de teclat
  scale = 1.0f;
  angle = 0.0f;
}

MyGLWidget::~MyGLWidget ()
{
  if (program != NULL)
    delete program;
}

void MyGLWidget::initializeGL ()
{
  // Cal inicialitzar l'ús de les funcions d'OpenGL
  initializeOpenGLFunctions();  

  glClearColor(0.5, 0.7, 1.0, 1.0); // defineix color de fons (d'esborrat)
  carregaShaders();
  createBuffers();
  
  glEnable(GL_DEPTH_TEST);
  ini_camera();
}

void MyGLWidget::paintGL () 
{
  // Esborrem el frame-buffer
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Carreguem la transformació de model
  modelTransformTerra();
  viewTransform();
  
  // Activem el VAO per a pintar el terra 
  glBindVertexArray (VAO_Terra);

  // pintem
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

  modelTransform();
  
  // Activem el VAO per a pintar el model
  glBindVertexArray (VAO_Model);

  // pintem
  glDrawArrays(GL_TRIANGLES, 0, m.faces().size()*3);
  
  
  modelTransformModel2();
  
  // Activem el VAO per a pintar el model
  glBindVertexArray (VAO_Model);

  // pintem
  glDrawArrays(GL_TRIANGLES, 0, m.faces().size()*3);

  glBindVertexArray (0);
}

void MyGLWidget::modelTransform () 
{
  // Matriu de transformació de model
  glm::mat4 transform (1.0f);
  transform = glm::translate(transform, glm::vec3(1.0, 0.5, 1.0));
  transform = glm::scale(transform, glm::vec3(scale));
  transform = glm::translate(transform, -center);
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &transform[0][0]);
}

void MyGLWidget::modelTransformTerra(){
  glm::mat4 TG(1.0); //Matriu de transformació
  TG = glm::translate(TG, glm::vec3(0.0,0.0,0.0));
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &TG[0][0]);
}

void MyGLWidget::modelTransformModel2 () 
{
  // Matriu de transformació de model
  glm::mat4 transform (1.0f);
  transform = glm::translate(transform, glm::vec3(-1.0, 0.5, -1.0));
  transform = glm::rotate(transform, float(M_PI), glm::vec3(0, 1, 0));
  transform = glm::scale(transform, glm::vec3(scale));
  transform = glm::translate(transform, -center);
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &transform[0][0]);
}

void MyGLWidget::projectTransform() 
{
  glm::mat4 Proj=glm::perspective(FOV, ra, znear, zfar);
  glUniformMatrix4fv(projLoc, 1, GL_FALSE, &Proj[0][0]);
}

void MyGLWidget::viewTransform() {
  //glm::mat4 View = glm::lookAt(OBS, VRP, UP);
  
  glm::mat4 View(1.0f);
  
  //Allunyar observador
  View = glm::translate(View,glm::vec3(0, 0, -d));
  
  //Rotació
  View = glm::rotate(View, psi, glm::vec3(0,1,0));
  View = glm::rotate(View, theta, glm::vec3(1,0,0));
  View = glm::rotate(View, -phi, glm::vec3(0,0,1));
  
  //Gir de 45 graus en l'eix X per veure bé l'escena
  View = glm::rotate(View, (float)M_PI/4, glm::vec3(1,0,0));
  
  //Centrar Objecte
  View = glm::translate(View, -VRP);
  
  glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &View[0][0]);
}

void MyGLWidget::ini_camera() 
{  
  calculaCapsaContenidora();
  
  d = 1.5f*rad;
  
  znear = (d-rad)/2.0f;
  zfar = d+rad;
  
  //angle = float(asin(rad / d));
  angle = float(atan(0.5f / znear));
  FOV = 2.0f*angle;
  
  ra = float(width()) / float(height());

  OBS = glm::vec3(0,0,d);
  VRP = glm::vec3(0,0,0);
  UP = glm::vec3(0,1,0);
  
  psi = theta = phi = 0.0f;
  
  xClick = 0;
  yClick = 0;
  deltaA = M_PI / 180;
  
  //modelTransform();
  projectTransform();
  viewTransform();
}

void MyGLWidget::get_cent_rad(glm::vec3 v_min, glm::vec3 v_max) 
{
  center = (v_max + v_min)/2.0f;
  
  double x = pow((v_max[0] - v_min[0]),2);
  double y = pow((v_max[1] - v_min[1]),2);
  double z = pow((v_max[2] - v_min[2]),2);
  rad = sqrt(x + y + z)/2.0f;
}

void MyGLWidget::calculaCapsaContenidora() {
  glm::vec3 v_min, v_max;
  v_min = v_max = glm::vec3(m.vertices()[0], 
			    m.vertices()[1], 
			    m.vertices()[2]);
  for (unsigned int i = 3; i < m.vertices().size(); i+=3) {
    if (m.vertices()[i] < v_min[0]) v_min[0] = m.vertices()[i];
    if (m.vertices()[i + 1] < v_min[1]) v_min[1] = m.vertices()[i + 1];
    if (m.vertices()[i + 2] < v_min[2]) v_min[2] = m.vertices()[i + 2];
    
    if (m.vertices()[i] > v_max[0]) v_max[0] = m.vertices()[i];
    if (m.vertices()[i + 1] > v_max[1]) v_max[1] = m.vertices()[i + 1];
    if (m.vertices()[i + 2] > v_max[2]) v_max[2] = m.vertices()[i + 2];
  }
  
  scale = float(1.0/(v_max[1] - v_min[1]));
  
  get_cent_rad(v_min, v_max);
}

void MyGLWidget::resizeGL (int w, int h) 
{
  glViewport(0, 0, w, h);
  
  //Calculem el nou ra i cridem a perspective
  ra = float(w) / float(h);
  
  if (h > w) {
    double alpha = (double)(atan((tan(angle)/ra)));
    FOV = alpha*2.0f;
  }
  
  projectTransform();
}

void MyGLWidget::keyPressEvent(QKeyEvent* event) 
{
  makeCurrent();
  switch (event->key()) {
    case Qt::Key_S: { // escalar a més gran
      scale += 0.05;
      break;
    }
    case Qt::Key_D: { // escalar a més petit
      scale -= 0.05;
      break;
    }
    case Qt::Key_R: {
      angle += (float)M_PI/4;
      break;
    }
    case Qt::Key_Z: { //Zoom-in
      FOV -= deltaA;
      projectTransform();
      break;
    }
    case Qt::Key_X: { //Zoom-out
      FOV += deltaA;
      projectTransform();
      break;
    }
    default: event->ignore(); break;
  }
  update();
}

void MyGLWidget::createBuffers () 
{
  //Terra
  
  glm::vec3 Vertices[4];  // Tres vèrtexs amb X, Y i Z
  Vertices[0] = glm::vec3(-2.0,0.0,2.0);
  Vertices[1] = glm::vec3(-2.0,0.0,-2.0);
  Vertices[3] = glm::vec3(2.0,0.0,-2.0);
  Vertices[2] = glm::vec3(2.0,0.0,2.0);
  
  glm::vec3 Color[4];  // Tres vèrtexs amb X, Y i Z
  Color[0] = glm::vec3(0.0,0.0,1.0);
  Color[1] = glm::vec3(0.0,0.0,1.0);
  Color[2] = glm::vec3(0.0,0.0,1.0);
  Color[3] = glm::vec3(0.0,0.0,1.0);
  
  // Creació del Vertex Array Object per pintar
  glGenVertexArrays(1, &VAO_Terra);
  glBindVertexArray(VAO_Terra);
  
  // Creació del buffer amb les dades dels vèrtexs
  glGenBuffers(1, &VBO_TerraPos);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_TerraPos);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Vertices, GL_STATIC_DRAW);
  
  // Activem l'atribut vertexLoc
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(vertexLoc);
  
  glGenBuffers(1, &VBO_TerraCol);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_TerraCol);
  glBufferData(GL_ARRAY_BUFFER, sizeof(Vertices), Color, GL_STATIC_DRAW);

  // Activem l'atribut colorLoc
  glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(colorLoc);  
  
  
  //Model
  
  //Carreguem el model
  m.load("models/Patricio.obj");
  
  int size_buffers = sizeof(GL_FLOAT)*m.faces().size()*3*3;

  // Creació del Vertex Array Object per pintar
  glGenVertexArrays(1, &VAO_Model);
  glBindVertexArray(VAO_Model);

  glGenBuffers(1, &VBO_ModelPos);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_ModelPos);
  glBufferData(GL_ARRAY_BUFFER, size_buffers, m.VBO_vertices(), GL_STATIC_DRAW);

  // Activem l'atribut vertexLoc
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(vertexLoc);

  glGenBuffers(1, &VBO_ModelCol);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_ModelCol);
  glBufferData(GL_ARRAY_BUFFER, size_buffers, m.VBO_matdiff(), GL_STATIC_DRAW);

  // Activem l'atribut colorLoc
  glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(colorLoc);

  glBindVertexArray (0);
}

void MyGLWidget::carregaShaders()
{
  // Creem els shaders per al fragment shader i el vertex shader
  QOpenGLShader fs (QOpenGLShader::Fragment, this);
  QOpenGLShader vs (QOpenGLShader::Vertex, this);
  // Carreguem el codi dels fitxers i els compilem
  fs.compileSourceFile("shaders/fragshad.frag");
  vs.compileSourceFile("shaders/vertshad.vert");
  // Creem el program
  program = new QOpenGLShaderProgram(this);
  // Li afegim els shaders corresponents
  program->addShader(&fs);
  program->addShader(&vs);
  // Linkem el program
  program->link();
  // Indiquem que aquest és el program que volem usar
  program->bind();

  // Obtenim identificador per a l'atribut “vertex” del vertex shader
  vertexLoc = glGetAttribLocation (program->programId(), "vertex");
  // Obtenim identificador per a l'atribut “color” del vertex shader
  colorLoc = glGetAttribLocation (program->programId(), "color");
  // Uniform locations
  transLoc = glGetUniformLocation(program->programId(), "TG");
  projLoc = glGetUniformLocation(program->programId(), "proj");
  viewLoc = glGetUniformLocation(program->programId(), "view");
}

void MyGLWidget::mousePressEvent(QMouseEvent *e) 
{
  if (e->button() and Qt::LeftButton) {
    xClick = e->x();
    yClick = e->y();
  }  
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *e) 
{
  int dx = abs(e->x() - xClick);
  int dy = abs(e->y() - yClick);
  float inc;
  
  if (dx > dy) { //Gir respecte eixY
    inc = float(abs(e->x() - xClick) * deltaA);
    if (e->x() > xClick) {
      psi += inc;
    }
    else psi -= inc;
    
  }
  else { //Gir respecte eixX
    inc = float(abs(e->y() - yClick) * deltaA);
    if (e->y() > yClick) {
      theta += inc;
    }
    else theta -= inc;
  }
  update();
  
  xClick = e->x();
  yClick = e->y();
}