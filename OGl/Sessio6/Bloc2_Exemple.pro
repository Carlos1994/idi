TEMPLATE    = app
QT         += opengl 

INCLUDEPATH +=  /usr/include/glm \
		/home/carlos/UPC/IDI/OGl/Sessio6

FORMS += MyForm.ui

HEADERS += MyForm.h MyGLWidget.h \
	   model.h

SOURCES += main.cpp MyForm.cpp \
        MyGLWidget.cpp \
        model.cpp
