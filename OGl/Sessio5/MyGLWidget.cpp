#include "MyGLWidget.h"

#include <iostream>

MyGLWidget::MyGLWidget (QWidget* parent) : QOpenGLWidget(parent)
{
  setFocusPolicy(Qt::ClickFocus);  // per rebre events de teclat
  scale = 1.0f;
  angle = 0.0f;
}

MyGLWidget::~MyGLWidget ()
{
  if (program != NULL)
    delete program;
}

void MyGLWidget::initializeGL ()
{
  // Cal inicialitzar l'ús de les funcions d'OpenGL
  initializeOpenGLFunctions();  

  glClearColor(0.5, 0.7, 1.0, 1.0); // defineix color de fons (d'esborrat)
  carregaShaders();
  createBuffers();
  
  glEnable(GL_DEPTH_TEST);
  ini_camera();
}

void MyGLWidget::paintGL () 
{
  // Esborrem el frame-buffer
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  // Carreguem la transformació de model
  modelTransform ();
  viewTransform();

  // Activem el VAO per a pintar la caseta 
  glBindVertexArray (VAO_Homer);

  // pintem
  glDrawArrays(GL_TRIANGLES, 0, m.faces().size()*3);

  glBindVertexArray (0);
}

void MyGLWidget::modelTransform () 
{
  // Matriu de transformació de model
  glm::mat4 transform (1.0f);
  transform = glm::scale(transform, glm::vec3(scale));
  transform = glm::translate(transform, -center);
  glUniformMatrix4fv(transLoc, 1, GL_FALSE, &transform[0][0]);
}

void MyGLWidget::projectTransform() 
{
  glm::mat4 Proj=glm::perspective(FOV, ra, znear, zfar);
  glUniformMatrix4fv(projLoc, 1, GL_FALSE, &Proj[0][0]);
}

void MyGLWidget::viewTransform() {
  //glm::mat4 View = glm::lookAt(OBS, VRP, UP);
  
  glm::mat4 View(1.0f);
  View = glm::translate(View,glm::vec3(0, 0, -d));
  
  //Rotació
  View = glm::rotate(View, -psi, glm::vec3(0,1,0));
  View = glm::rotate(View, theta, glm::vec3(1,0,0));
  View = glm::rotate(View, -phi, glm::vec3(0,0,1));
  
  
  //View = glm::rotate(View, (float)(M_PI/2.0f), glm::vec3(0,1,0));
  
  View = glm::translate(View, -VRP);
  glUniformMatrix4fv(viewLoc, 1, GL_FALSE, &View[0][0]);
}

void MyGLWidget::ini_camera() 
{  
  calculaCapsaContenidora();
  
  d = 1.5f*rad;
  
  angle = float(asin(rad / d));
  FOV = 2.0f*angle;
  
  ra = float(width()) / float(height());
  scale = 1.0f;
  
  znear = (d-rad)/2.0f;
  zfar = d+rad;
  
  OBS = glm::vec3(0,0,d);
  VRP = glm::vec3(0,0,0);
  UP = glm::vec3(0,1,0);
  
  psi = theta = phi = 0.0f;
  
  xClick = 0;
  yClick = 0;
  deltaA = M_PI / 180;
  
  //modelTransform();
  projectTransform();
  viewTransform();
}

void MyGLWidget::get_cent_rad(glm::vec3 v_min, glm::vec3 v_max) 
{
  center = (v_max + v_min)/2.0f;
  
  double x = pow((v_max[0] - v_min[0]),2);
  double y = pow((v_max[1] - v_min[1]),2);
  double z = pow((v_max[2] - v_min[2]),2);
  rad = sqrt(x + y + z)/2.0f;
}

void MyGLWidget::calculaCapsaContenidora() {
  glm::vec3 v_min, v_max;
  v_min = v_max = glm::vec3(m.vertices()[0], 
			    m.vertices()[1], 
			    m.vertices()[2]);
  for (unsigned int i = 3; i < m.vertices().size(); i+=3) {
    if (m.vertices()[i] < v_min[0]) v_min[0] = m.vertices()[i];
    if (m.vertices()[i + 1] < v_min[1]) v_min[1] = m.vertices()[i + 1];
    if (m.vertices()[i + 2] < v_min[2]) v_min[2] = m.vertices()[i + 2];
    
    if (m.vertices()[i] > v_max[0]) v_max[0] = m.vertices()[i];
    if (m.vertices()[i + 1] > v_max[1]) v_max[1] = m.vertices()[i + 1];
    if (m.vertices()[i + 2] > v_max[2]) v_max[2] = m.vertices()[i + 2];
  }
  
  /*std::cout << "Min" << std::endl;
  std::cout << "X: " << v_min[0] << " Y: " << v_min[1] << " Z: " << v_min[2] << std::endl;
  std::cout << "Max" << std::endl;
  std::cout << "X: " << v_max[0] << " Y: " << v_max[1] << " Z: " << v_max[2] << std::endl;*/
  
  get_cent_rad(v_min, v_max);
  
  /*std::cout << "Rad: " << rad << std::endl;
  std::cout << "CX: " << center[0] << " CY: " << center[1] << " CZ: " << center[2] << std::endl;*/
}

void MyGLWidget::resizeGL (int w, int h) 
{
  glViewport(0, 0, w, h);
  
  //Calculem el nou ra i cridem a perspective
  ra = float(w) / float(h);
  
  if (h > w) {
    double alpha = (double)(atan((tan(angle)/ra)));
    FOV = alpha*2.0f;
  }
  
  projectTransform();
}

void MyGLWidget::keyPressEvent(QKeyEvent* event) 
{
  makeCurrent();
  switch (event->key()) {
    case Qt::Key_S: { // escalar a més gran
      scale += 0.05;
      break;
    }
    case Qt::Key_D: { // escalar a més petit
      scale -= 0.05;
      break;
    }
    case Qt::Key_R: {
      angle += (float)M_PI/4;
      break;
    }
    case Qt::Key_Z: { //Zoom-in
      FOV -= deltaA;
      projectTransform();
      break;
    }
    case Qt::Key_X: { //Zoom-out
      FOV += deltaA;
      projectTransform();
      break;
    }
    default: event->ignore(); break;
  }
  update();
}

void MyGLWidget::createBuffers () 
{
  //Carreguem el model
  m.load("Patricio.obj");
  
  int size_buffers = sizeof(GL_FLOAT)*m.faces().size()*3*3;

  // Creació del Vertex Array Object per pintar
  glGenVertexArrays(1, &VAO_Homer);
  glBindVertexArray(VAO_Homer);

  glGenBuffers(1, &VBO_HomerPos);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_HomerPos);
  glBufferData(GL_ARRAY_BUFFER, size_buffers, m.VBO_vertices(), GL_STATIC_DRAW);

  // Activem l'atribut vertexLoc
  glVertexAttribPointer(vertexLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(vertexLoc);

  glGenBuffers(1, &VBO_HomerCol);
  glBindBuffer(GL_ARRAY_BUFFER, VBO_HomerCol);
  glBufferData(GL_ARRAY_BUFFER, size_buffers, m.VBO_matdiff(), GL_STATIC_DRAW);

  // Activem l'atribut colorLoc
  glVertexAttribPointer(colorLoc, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(colorLoc);

  glBindVertexArray (0);
}

void MyGLWidget::carregaShaders()
{
  // Creem els shaders per al fragment shader i el vertex shader
  QOpenGLShader fs (QOpenGLShader::Fragment, this);
  QOpenGLShader vs (QOpenGLShader::Vertex, this);
  // Carreguem el codi dels fitxers i els compilem
  fs.compileSourceFile("shaders/fragshad.frag");
  vs.compileSourceFile("shaders/vertshad.vert");
  // Creem el program
  program = new QOpenGLShaderProgram(this);
  // Li afegim els shaders corresponents
  program->addShader(&fs);
  program->addShader(&vs);
  // Linkem el program
  program->link();
  // Indiquem que aquest és el program que volem usar
  program->bind();

  // Obtenim identificador per a l'atribut “vertex” del vertex shader
  vertexLoc = glGetAttribLocation (program->programId(), "vertex");
  // Obtenim identificador per a l'atribut “color” del vertex shader
  colorLoc = glGetAttribLocation (program->programId(), "color");
  // Uniform locations
  transLoc = glGetUniformLocation(program->programId(), "TG");
  projLoc = glGetUniformLocation(program->programId(), "proj");
  viewLoc = glGetUniformLocation(program->programId(), "view");
}

void MyGLWidget::mousePressEvent(QMouseEvent *e) 
{
  if (e->button() and Qt::LeftButton) {
    xClick = e->x();
    yClick = e->y();
  }  
}

void MyGLWidget::mouseMoveEvent(QMouseEvent *e) 
{
  int dx = abs(e->x() - xClick);
  int dy = abs(e->y() - yClick);
  float inc;
  
  if (dx > dy) { //Gir respecte eixY
    inc = float(abs(e->x() - xClick) * deltaA);
    if (e->x() > xClick) {
      psi += inc;
    }
    else psi -= inc;
    
  }
  else { //Gir respecte eixX
    inc = float(abs(e->y() - yClick) * deltaA);
    if (e->y() > yClick) {
      theta += inc;
    }
    else theta -= inc;
  }
  viewTransform();
  update();
  
  xClick = e->x();
  yClick = e->y();
}

