TEMPLATE    = app
QT         += opengl 

INCLUDEPATH +=  /usr/include/glm \
		/dades/carlos.lerida/linux/IDI/OGl/Sessio5

FORMS += MyForm.ui

HEADERS += MyForm.h MyGLWidget.h \
	   /dades/carlos.lerida/linux/IDI/OGl/Sessio5/model.h

SOURCES += main.cpp MyForm.cpp \
        MyGLWidget.cpp \
        /dades/carlos.lerida/linux/IDI/OGl/Sessio5/model.cpp
