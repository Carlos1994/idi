TEMPLATE    = app
QT         += opengl 

INCLUDEPATH +=  /usr/include/glm \
		/dades/carlos.lerida/linux/IDI/OGl/Sessio4

FORMS += MyForm.ui

HEADERS += MyForm.h MyGLWidget.h \
	   /dades/carlos.lerida/linux/IDI/OGl/Sessio4/model.h

SOURCES += main.cpp MyForm.cpp \
        MyGLWidget.cpp \
        /dades/carlos.lerida/linux/IDI/OGl/Sessio4/model.cpp
