TEMPLATE    = app
QT         += opengl 

INCLUDEPATH +=  /usr/include/glm \
		/home/carlos/UPC/IDI/ExercicisQT/Part2/Sessio7

FORMS += MyForm.ui

HEADERS += MyForm.h MyGLWidget.h \
	   model.h

SOURCES += main.cpp MyForm.cpp \
        MyGLWidget.cpp \
        model.cpp
