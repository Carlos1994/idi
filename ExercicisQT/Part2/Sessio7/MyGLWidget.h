#define GLM_FORCE_RADIANS
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLWidget>
#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QKeyEvent>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "model.h"
#include <cmath>
//#include <vector>

class MyGLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core 
{
  Q_OBJECT

  public:
    MyGLWidget (QWidget *parent=0);
    ~MyGLWidget ();

  protected:
    // initializeGL - Aqui incluim les inicialitzacions del contexte grafic.
    virtual void initializeGL ( );
    // paintGL - Mètode cridat cada cop que cal refrescar la finestra.
    // Tot el que es dibuixa es dibuixa aqui.
    virtual void paintGL ( );
    // resizeGL - És cridat quan canvia la mida del widget
    virtual void resizeGL (int width, int height);
    // keyPressEvent - Es cridat quan es prem una tecla
    virtual void keyPressEvent (QKeyEvent *event);
    
    //mouse events
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseMoveEvent(QMouseEvent *event);

  private:
    void createBuffers ();
    void createBuffersModel(Model &m);
    void carregaShaders ();
    void modelTransform ();
    void modelTransformTerra ();
    void modelTransformModel2 ();
    void projectTransform_perspective();
    void projectTransform_ortho();
    void viewTransform();
    void ini_camera();
    void get_cent_rad(glm::vec3 v_min, glm::vec3 v_max);
    void calculaCapsaContenidora(Model &m);

    // attribute locations
    GLuint vertexLoc, colorLoc;
    // uniform locations
    GLuint transLoc, projLoc, viewLoc;
    // VAO i VBO names
    GLuint VAO_Model, VBO_ModelPos, VBO_ModelCol;
    GLuint VAO_Model1, VBO_ModelPos1, VBO_ModelCol1;
    GLuint VAO_Terra, VBO_TerraPos, VBO_TerraCol;
    // Program
    QOpenGLShaderProgram *program;
    // Internal vars
    float scale, angle;
    glm::vec3 pos;
    
    //Global vars
    glm::vec3 OBS, VRP, UP;
    float FOV, ra, znear, zfar, d;
    
    float left, right, bottom, top;
    
    float psi, theta, phi; // angles d’Euler
    
    float deltaA; //Increment angles
    
    glm::vec3 center;
    float rad;
    
    //Model m;
    
    Model patricio, legoman;
    
    //Mouse event
    int xClick, yClick;
    
    enum Camera { ORTHO, PERSPECTIVE };
    Camera c;
    
    enum Mod { PATRICIO, LEGOMAN};
    Mod actual_mod;
    
  public slots:
  
	void changeCamera();
	
	void changeModel();
	
	void modifyFOV(int newFOV);
    
  signals:

};

