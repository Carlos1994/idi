#define GLM_FORCE_RADIANS
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLWidget>
#include <QOpenGLShader>
#include <QOpenGLShaderProgram>
#include <QKeyEvent>
#include <QMouseEvent>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "model.h"

class MyGLWidget : public QOpenGLWidget, protected QOpenGLFunctions_3_3_Core 
{
  Q_OBJECT

  public:
    MyGLWidget (QWidget *parent=0);
    ~MyGLWidget ();

  protected:
    // initializeGL - Aqui incluim les inicialitzacions del contexte grafic.
    virtual void initializeGL ( );
    // paintGL - Mètode cridat cada cop que cal refrescar la finestra.
    // Tot el que es dibuixa es dibuixa aqui.
    virtual void paintGL ( );
    // resizeGL - És cridat quan canvia la mida del widget
    virtual void resizeGL (int width, int height);
    // keyPressEvent - Es cridat quan es prem una tecla
    virtual void keyPressEvent (QKeyEvent *event);
    // mouse{Press/Release/Move}Event - Són cridades quan es realitza l'event 
    // corresponent de ratolí
    virtual void mousePressEvent (QMouseEvent *event);
    virtual void mouseReleaseEvent (QMouseEvent *event);
    virtual void mouseMoveEvent (QMouseEvent *event);

  private:
    void createBuffers ();
    void carregaShaders ();
    void projectTransform ();
    void viewTransform ();
    void modelTransformTerra ();
    void modelTransformPatricio ();
    void modelTransformPatricio2 ();
    void modelTransformCow ();
    void calculaCapsaModel (Model &m, std::string model_act);
    void initializeCamera();

    // VAO i VBO names
    GLuint VAO_Cow, VBO_CowPos, VBO_CowNorm, VBO_CowMatamb, VBO_CowMatdiff, VBO_CowMatspec, VBO_CowMatshin;
    GLuint VAO_Patr, VBO_PatrPos, VBO_PatrNorm, VBO_PatrMatamb, VBO_PatrMatdiff, VBO_PatrMatspec, VBO_PatrMatshin;
    GLuint VAO_Terra, VBO_TerraPos, VBO_TerraNorm, VBO_TerraMatamb, VBO_TerraMatdiff, VBO_TerraMatspec, VBO_TerraMatshin;
    // Program
    QOpenGLShaderProgram *program;
    // uniform locations
    GLuint transLoc, projLoc, viewLoc;
    // attribute locations
    GLuint vertexLoc, normalLoc, matambLoc, matdiffLoc, matspecLoc, matshinLoc;
    
    // Per indicar quan pintem la vaca
    GLuint vacaLoc;
    int vaca;

    //Parametres camera
    float FOV, ra, znear, zfar, d, radi;
    glm::vec3 center;
    
    // model
    Model patr, cow;
    
    // paràmetres calculats a partir de la capsa contenidora del model
    glm::vec3 centreModel[2];
    float escalaCow, escalaPatr, angle;
    // radi de l'escena
    float radiEsc;

    typedef  enum {NONE, ROTATE, ZOOM} InteractiveAction;
    InteractiveAction DoingInteractive;
    int xClick, yClick;
    float angleY, angleX;
    bool perspectiva;
    
    float girY;
    
    typedef enum {NORMAL, CANTONADA} tipusCamera;
    tipusCamera camera;
    
    GLuint xPressedLoc, lPressedLoc;
    int xPressed, lPressed;
    
public slots:
    
    void changeZoom(int newFOV);
    
    void focusEscena();
    
    void focusCamera();
    
signals:
    
    void clickFocusEscena();
    
    void clickFocusCamera();
};

