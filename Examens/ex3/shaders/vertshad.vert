#version 330 core

vec3 white = vec3(1.0,1.0,1.0);
vec3 cian = vec3(0.0, 0.69, 0.965);

vec3 escena = vec3(1,1,1);
vec3 camera = vec3(0,0,0);

in vec3 vertex;
in vec3 normal;

in vec3 matambmod;
in vec3 matdiffmod;
in vec3 matspecmod;
in float matshinmod;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 TG;

uniform int lPressed;

vec3 matamb;
vec3 matdiff;
vec3 matspec;
float matshin;

// Valors per als components que necessitem dels focus de llum
vec3 colFocus = vec3(0.8,0.8,0.8); //ColFocus del codi original
vec3 llumAmbient = vec3(0.2, 0.2, 0.2);
uniform vec3 posFocus; // en SCA

out vec3 fcolor;

vec3 Lambert (vec3 NormSCO, vec3 L) 
{
    // S'assumeix que els vectors que es reben com a paràmetres estan normalitzats

    // Inicialitzem color a component ambient
    vec3 colRes = llumAmbient * matamb;

    // Afegim component difusa, si n'hi ha
    if (dot (L, NormSCO) > 0)
      colRes = colRes + colFocus * matdiff * dot (L, NormSCO);
    return (colRes);
}

vec3 Phong (vec3 NormSCO, vec3 L, vec4 vertSCO) 
{
    // Els vectors estan normalitzats

    // Inicialitzem color a Lambert
    vec3 colRes = Lambert (NormSCO, L);

    // Calculem R i V
    if (dot(NormSCO,L) < 0)
      return colRes;  // no hi ha component especular

    vec3 R = reflect(-L, NormSCO); // equival a: normalize (2.0*dot(NormSCO,L)*NormSCO - L);
    vec3 V = normalize(-vertSCO.xyz);

    if ((dot(R, V) < 0) || (matshin == 0))
      return colRes;  // no hi ha component especular
    
    // Afegim la component especular
    float shine = pow(max(0.0, dot(R, V)), matshin);
    return (colRes + matspec * colFocus * shine); 
}

void main()
{	
    fcolor = matdiff;
    gl_Position = proj * view * TG * vec4 (vertex, 1.0);
    
    matamb = matambmod;
    matdiff = matdiffmod;
    matspec = matspecmod;
    matshin = matshinmod;
    
    //Llum de camera    
    vec4 vertex_SCO = view*TG*vec4(vertex, 1.0);
    mat3 NormalMatrix = inverse(transpose(mat3 (view * TG)));
    vec3 normal_SCO = NormalMatrix*normal;
    
    vec4 posFocus_SCO;
    if (lPressed == 1) {
      posFocus_SCO = view*vec4(posFocus, 1.0);
      colFocus = white;
    }
    else {
      posFocus_SCO = vec4(posFocus, 1.0);
      colFocus = cian;
    }
    
    vec4 L = posFocus_SCO - vertex_SCO;
    vec3 L_norm = normalize(L.xyz);
    normal_SCO = normalize(normal_SCO);
    
    //Phong
    vertex_SCO = normalize(vertex_SCO);
    fcolor = Phong(normal_SCO, L_norm, vertex_SCO); 
}
