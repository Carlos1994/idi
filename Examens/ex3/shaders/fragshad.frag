#version 330 core

in vec3 fcolor;
out vec4 FragColor;

uniform int xPressed;

vec4 black = vec4(0.0,0.0,0.0,1.0);
vec4 white = vec4(1.0,1.0,1.0,1.0);

void main()
{	
    if (xPressed == 1) {
        if (mod(gl_FragCoord.y, 20.0) <= 10.0) FragColor = black;
        else FragColor = white;
    }
	else FragColor = vec4(fcolor,1);	
}
